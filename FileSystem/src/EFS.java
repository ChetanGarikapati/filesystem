import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.security.spec.KeySpec;

import static java.util.Arrays.copyOfRange;
import static java.util.Base64.getEncoder;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 * @author Chetan Garikapati
 * NwtId - CXG190009
 * Email - cxg190009@utdallas.edu
 */
public class EFS extends Utility {

    private static final String META_DATA_FILE = "meta_data";
    private static final String NEW_LINE = "\n";

    public EFS(Editor e) {
        super(e);
        set_username_password();
    }


    /**
     * Steps to consider... <p>
     * - add padded username and password salt to header <p>
     * - add password hash and file length to secret data <p>
     * - AES encrypt padded secret data <p>
     * - add header and encrypted secret data to metadata <p>
     * - compute HMAC for integrity check of metadata <p>
     * - add metadata and HMAC to metadata file block <p>
     */
    @Override
    public void create(String file_name, String user_name, String password) throws Exception {
        dir = new File(file_name);
    }

    /**
     * Derives key using key stretching algorithm
     *
     * @return
     */
    private SecretKey generateKeyFromPassword() {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(password.toCharArray(), "salt".getBytes(), 65536, 256);
            return new SecretKeySpec(factory.generateSecret(spec)
                    .getEncoded(), "AES");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Steps to consider... <p>
     * - check if metadata file size is valid <p>
     * - get username from metadata <p>
     */
    @Override
    public String findUser(String file_name) throws Exception {
        return getMetaData(file_name).split(NEW_LINE)[0];
    }

    /**
     * Decrypts the meta data file and returns it
     *
     * @param file_name
     * @return
     * @throws Exception
     */
    private String getMetaData(String file_name) throws Exception {
        File fileToReadMetaData = new File(file_name, META_DATA_FILE);
        byte[] decriptAes = decript_AES(read_from_file(fileToReadMetaData), generateKeyFromPassword().getEncoded());
        return byteArray2String(decriptAes);
    }

    /**
     * Steps to consider...:<p>
     * - get password, salt then AES key <p>
     * - decrypt password hash out of encrypted secret data <p>
     * - check the equality of the two password hash values <p>
     * - decrypt file length out of encrypted secret data
     */
    @Override
    public int length(String file_name, String password) throws Exception {
        try {
            return Integer.parseInt(getMetaData(file_name).split(NEW_LINE)[2]);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            throw new PasswordIncorrectException();
        }
    }

    /**
     * Steps to consider...:<p>
     * - verify password <p>
     * - check check if requested starting position and length are valid <p>
     * - decrypt content data of requested length
     */
    @Override
    public byte[] read(String file_name, int starting_position, int len, String password) throws Exception {
        String[] metadataInfo = getMetaData(file_name).split(NEW_LINE);
        if (metadataInfo[0].equals(username) && metadataInfo[1].equals(password))
            return decrypt(file_name, starting_position, len, password, metadataInfo);
        else
            throw new PasswordIncorrectException();
    }

    /**
     * Decrypts the file
     *
     * @param file_name
     * @param starting_position
     * @param len
     * @param password
     * @param metadataInfo
     * @return
     * @throws Exception
     */
    private byte[] decrypt(String file_name, int starting_position, int len, String password, String[] metadataInfo) throws Exception {
        try {
            int fileSize;
            int ivCount = 0;

            byte[] restoredIV = new byte[1023];
            File fileToRead = new File(file_name);

            fileSize = Integer.parseInt(metadataInfo[2]);
            String savedIVString = metadataInfo[3];
            for (String ivBit : savedIVString.trim().split(",")) {
                try {
                    restoredIV[ivCount++] = Byte.parseByte(ivBit);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error in : " + ivBit);
                }
            }

            fileSize = fileSize / Config.BLOCK_SIZE;
            ByteBuffer cipherText = ByteBuffer.allocate((fileSize + 1) * Config.BLOCK_SIZE);
            cipherText.position(0);

            for (int i = 0; i <= fileSize; i++) {
                byte[] bytes = read_from_file(new File(fileToRead, Integer.toString(i)));
                cipherText.put(bytes);
            }

            cipherText.position(0);
            ByteBuffer counterInfo = ByteBuffer.allocate(Config.BLOCK_SIZE);
            counterInfo.put(restoredIV);

            for (int i = 0; i <= fileSize; i++) {
                counterInfo.put(1023, Byte.valueOf(Integer.toString(i)));
                byte[] cipher = Utility.encript_AES(counterInfo.array(), generateKeyFromPassword().getEncoded());

                for (int j = i * Config.BLOCK_SIZE, k = 0; k < Config.BLOCK_SIZE && cipherText.remaining() != 0; j++, k++) {
                    cipherText.put(j, (byte) (cipherText.get() ^ cipher[k]));
                }
            }

            return new String(cipherText.array()).substring(0, len).getBytes();

        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
            showMessageDialog(null, "File corrupted or modified");
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }


    /**
     * Steps to consider...:<p>
     * - verify password <p>
     * - check check if requested starting position and length are valid <p>
     * - ### main procedure for update the encrypted content ### <p>
     * - compute new HMAC and update metadata
     */
    @Override
    public void write(String file_name, int starting_position, byte[] content, String password) throws Exception {
        encrypt(file_name, starting_position, content, password);
    }

    /**
     * Encrypts the plain text with AES-128 using CTR mode
     *
     * @param file_name
     * @param starting_position
     * @param content
     * @param password
     * @throws Exception
     */
    private void encrypt(String file_name, int starting_position, byte[] content, String password) throws Exception {
        try {
            File fileToWrite = new File(file_name);
            int fileLength = content.length / Config.BLOCK_SIZE;
            byte[] initializationVector = secureRandomNumber(1023);

            if (starting_position > 0)
                content = manageInsertions(file_name, starting_position, content);

            ByteBuffer inputBuffer = ByteBuffer.allocate(content.length);
            inputBuffer.put(content);
            ByteBuffer counterInfo = ByteBuffer.allocate(Config.BLOCK_SIZE);
            counterInfo.put(initializationVector);

            inputBuffer.position(0);

            for (int i = 0; i <= fileLength; i++) {
                counterInfo.put(1023, Byte.valueOf(Integer.toString(i)));

                byte[] cipherText = Utility.encript_AES(counterInfo.array(), generateKeyFromPassword().getEncoded());

                for (int j = i * Config.BLOCK_SIZE, k = 0; k < Config.BLOCK_SIZE && inputBuffer.remaining() != 0; j++, k++) {
                    inputBuffer.put(j, (byte) (inputBuffer.get() ^ cipherText[k]));
                }

                save_to_file(copyOfRange(inputBuffer.array(), i * Config.BLOCK_SIZE, (i + 1) * Config.BLOCK_SIZE),
                        new File(fileToWrite, Integer.toString(i)));
            }

            updateMetaData(initializationVector, file_name, content);
            System.out.println("File saved");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private byte[] manageInsertions(String file_name, int starting_position, byte[] content) throws Exception {

        System.out.println("In manage insertions " + starting_position);

        try {
            String contentString = new String(content);

            int fileLength = length(file_name, password);
            if (fileLength >= starting_position) {
                byte[] read = read(file_name, 0, fileLength, password);
                String readString = new String(read);
                StringBuilder stringBuilder = new StringBuilder(readString);
                if (starting_position >= fileLength) stringBuilder.append(contentString);
                else stringBuilder.replace(starting_position, starting_position + content.length, contentString);
                return stringBuilder.toString().getBytes();
            }
        } catch (FileNotFoundException e) {
            showMessageDialog(null, "First save the file, then provide offset to save");
        }


        //THROWN BECAUSE OF INVALID SAVE POSITION
        throw new Exception();

    }

    /**
     * Steps to consider...:<p>
     * - verify password <p>
     * - check the equality of the computed and stored HMAC values for metadata and physical file blocks<p>
     */
    @Override
    public boolean check_integrity(String file_name, String password) throws Exception {
        try {
            String metaDataContent = getMetaData(file_name);
            byte[] fileContent = read(file_name, 0, length(file_name, password), password);

            System.out.println("integrity length : " + fileContent.length);
            String hashFromDecryptingFile = getEncoder().encodeToString(hash_SHA512(fileContent));
            String hashFromMetaData = metaDataContent.split(NEW_LINE)[4];

            String metaDataFileLength = metaDataContent.split(NEW_LINE)[5];
            String hashFromMetaDataFile = metaDataContent.split(NEW_LINE)[6];
            String hashFromDecryptingMetaFile = getEncoder()
                    .encodeToString(hash_SHA512(metaDataContent.substring(0, Integer.parseInt(metaDataFileLength) + 1 + metaDataFileLength.length()).getBytes()));

            System.out.println("decrypt value : " + hashFromDecryptingFile);
            System.out.println("Meta Data value : " + hashFromMetaData);

            System.out.println("meta - decrypt value : " + hashFromDecryptingMetaFile);
            System.out.println("meta - Meta Data value : " + hashFromMetaDataFile);
            return hashFromDecryptingFile.equals(hashFromMetaData);
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            return false;
        }
    }

    /**
     * Creates and save meta data of the file.
     *
     * @param initializationVector
     * @param file_name
     * @param content
     * @throws Exception
     */
    private void updateMetaData(byte[] initializationVector, String file_name, byte[] content) throws Exception {
        File fileToReadMetaData = new File(file_name, META_DATA_FILE);

        StringBuilder metaDataBuilder = new StringBuilder();
        metaDataBuilder.append(username).append(NEW_LINE).append(password);

        metaDataBuilder.append(NEW_LINE).append(content.length).append(NEW_LINE);
        for (byte b : initializationVector) {
            metaDataBuilder.append(b).append(",");
        }

        metaDataBuilder.append(NEW_LINE).append(getEncoder().encodeToString(hash_SHA512(content))); //FILE HASH
        metaDataBuilder.append(NEW_LINE).append(metaDataBuilder.length());
        metaDataBuilder.append(NEW_LINE);

        metaDataBuilder.append(getEncoder().encodeToString(hash_SHA512(metaDataBuilder.toString().getBytes())));
        metaDataBuilder.append(NEW_LINE);
        System.out.println("meta data length :" + metaDataBuilder.toString().getBytes().length);

        //PADDING
        while (metaDataBuilder.length() % 16 != 0) {
            metaDataBuilder.append("0");
        }

        save_to_file(encript_AES(metaDataBuilder.toString().getBytes(), generateKeyFromPassword().getEncoded()), fileToReadMetaData);

    }

    /**
     * Steps to consider... <p>
     * - verify password <p>
     * - truncate the content after the specified length <p>
     * - re-pad, update metadata and HMAC <p>
     */
    @Override
    public void cut(String file_name, int length, String password) throws Exception {
        byte[] read = read(file_name, 0, length, password);
        write(file_name, 0, read, password);
    }

}
