import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.spec.KeySpec;

public class Testing {

    byte[] iv;
    static String plainText = "The earliest modes of operation, ECB, CBC, OFB, and CFB (see below for all), date back to 1981 and were specified in FIPS 81, DES Modes of Operation. In 2001, the US National Institute of Standards and Technology (NIST) revised its list of approved modes of operation by including AES as a block cipher and adding CTR mode in SP800-38A, Recommendation for Block Cipher Modes of Operation. Finally, in January, 2010, NIST added XTS-AES in SP800-38E, Recommendation for Block Cipher Modes of Operation: The XTS-AES Mode for Confidentiality on Storage Devices. Other confidentiality modes exist which have not been approved by NIST. For example, CTS is ciphertext stealing mode and available in many popular cryptographic libraries.\n" +
            "\n" +
            "The block cipher modes ECB, CBC, OFB, CFB, CTR, and XTS provide confidentiality, but they do not protect against accidental modification or malicious tampering. Modification or tampering can be detected with a separate message authentication code such as CBC-MAC, or a digital signature. The cryptographic community recognized the need for dedicated integrity assurances and NIST responded with HMAC, CMAC, and GMAC. HMAC was approved in 2002 as FIPS 198, The Keyed-Hash Message Authentication Code (HMAC), CMAC was released in 2005 under SP800-38B, Recommendation for Block Cipher Modes of Operation: The CMAC Mode for Authentication, and GMAC was formalized in 2007 under SP800-38D, Recommendation for Block Cipher Modes of Operation: Galois/Counter Mode (GCM) and GMAC.\n" +
            "\n" +
            "The cryptographic community observed that compositing (combining) a confidentiality mode with an authenticity mode could be difficult and error prone. They therefore began to supply modes which combined confidentiality and data integrity into a single cryptographic primitive (an encryption algorithm). These combined modes are referred to as authenticated encryption, AE or \"authenc\". Examples of AE modes are CCM (SP800-38C), GCM (SP800-38D), CWC, EAX, IAPM, and OCB.\n" +
            "\n" +
            "Modes of operation are defined by a number of national and internationally recognized standards bodies. Notable standards organizations include NIST, ISO (with ISO/IEC 10116[5]), the IEC, the IEEE, ANSI, and the IETF.\n" +
            "Initialization vector (IV)\n" +
            "Main article: Initialization vector\n" +
            "\n" +
            "An initialization vector (IV) or starting variable (SV)[5] is a block of bits that is used by several modes to randomize the encryption and hence to produce distinct ciphertexts even if the same plaintext is encrypted multiple times, without the need for a slower re-keying process.[citation needed]\n" +
            "\n" +
            "An initialization vector has different security requirements than a key, so the IV usually does not need to be secret. For most block cipher modes it is important that an initialization vector is never reused under the same key, i.e. it must be a cryptographic nonce. Many block cipher modes have stronger requirements, such as the IV must be random or pseudorandom. Some block ciphers have particular problems with certain initialization vectors, such as all zero IV generating no encryption (for some keys).\n" +
            "\n" +
            "It is recommended to review relevant IV requirements for the particular block cipher mode in relevant specification, for example SP800-38A.\n" +
            "\n" +
            "For CBC and CFB, reusing an IV leaks some information about the first block of plaintext, and about any common prefix shared by the two messages.\n" +
            "\n" +
            "For OFB and CTR, reusing an IV causes key bitstream re-use, which breaks security.[8] This can be seen because both modes effectively create a bitstream that is XORed with the plaintext, and this bitstream is dependent on the key and IV only.\n" +
            "\n" +
            "In CBC mode, the IV must be unpredictable (random or pseudorandom) at encryption time; in particular, the (previously) common practice of re-using the last ciphertext block of a message as the IV for the next message is insecure (for example, this method was used by SSL 2.0). If an attacker knows the IV (or the previous block of ciphertext) before the next plaintext is specified, they can check their guess about plaintext of some block that was encrypted with the same key before (this is known as the TLS CBC IV attack).[9]\n" +
            "\n" +
            "For some keys an all-zero initialization vector may generate some block cipher modes (CFB-8, OFB-8) to get internal state stuck at all-zero. For CFB-8, an all-zero IV and an all-zero plaintext, causes 1/256 of keys to generate no encryption, plaintext is returned as ciphertext.[10] For OFB-8, using all zero initialization vector will generate no encryption for 1/256 of keys.[11] OFB-8 encryption returns the plaintext unencrypted for affected keys.\n" +
            "\n" +
            "Some modes (such as AES-SIV and AES-GCM-SIV) are built to be more nonce-misuse resistant, i.e. resilient to scenarios in which the randomness generation is faulty or under the control of the attacker.\n" +
            "\n" +
            "    Synthetic Initialization Vector (SIV) synthesize an internal IV by running an Pseudo-Random Function (PRF) construction called S2V on the input (additional data and plaintext), preventing any external data from directly controlling the IV. External nonces / IV may be feed into S2V as an additional data field.\n" +
            "    AES-GCM-SIV synthesize an internal IV by running POLYVAL Galois mode of authentication on input (additional data and plaintext), followed by an AES operation.\n" +
            "\n" +
            "Padding\n" +
            "Main article: Padding (cryptography)\n" +
            "\n" +
            "A block cipher works on units of a fixed size (known as a block size), but messages come in a variety of lengths. So some modes (namely ECB and CBC) require that the final block be padded before encryption. Several padding schemes exist. The simplest is to add null bytes to the plaintext to bring its length up to a multiple of the block size, but care must be taken that the original length of the plaintext can be recovered; this is trivial, for example, if the plaintext is a C style string which contains no null bytes except at the end. Slightly more complex is the original DES method, which is to add a single one bit, followed by enough zero bits to fill out the block; if the message ends on a block boundary, a whole padding block will be added. Most sophisticated are CBC-specific schemes such as ciphertext stealing or residual block termination, which do not cause any extra ciphertext, at the expense of some additional complexity. Schneier and Ferguson suggest two possibilities, both simple: append a byte with value 128 (hex 80), followed by as many zero bytes as needed to fill the last block, or pad the last block with n bytes all with value n.\n" +
            "\n" +
            "CFB, OFB and CTR modes do not require any special measures to handle messages whose lengths are not multiples of the block size, since the modes work by XORing the plaintext with the output of the block cipher. The last partial block of plaintext is XORed with the first few bytes of the last keystream block, producing a final ciphertext block that is the same size as the final partial plaintext block. This characteristic of stream ciphers makes them suitable for applications that require the encrypted ciphertext data to be the same size as the original plaintext data, and for applications that transmit data in streaming form where it is inconvenient to add padding bytes. ";

    private static SecretKey generateKeyFromPassword() {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec("password".toCharArray(), "salt".getBytes(), 65536, 256);
            return new SecretKeySpec(factory.generateSecret(spec)
                    .getEncoded(), "AES");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ByteBuffer encrypt(byte[] iv, ByteBuffer inputBuffer) {
        ByteBuffer counterInfo = ByteBuffer.allocate(Config.BLOCK_SIZE);
        counterInfo.put(iv);

        try {
            inputBuffer.position(0);
            int fileLength = inputBuffer.remaining() / Config.BLOCK_SIZE;
            System.out.println("File length : " + fileLength);

            for (int i = 0; i <= fileLength; i++) {
                counterInfo.put(1023, Byte.valueOf(Integer.toString(i)));

                byte[] cipherText = Utility.encript_AES(counterInfo.array(), generateKeyFromPassword().getEncoded());

                for (int j = i * Config.BLOCK_SIZE, k = 0; k < Config.BLOCK_SIZE && inputBuffer.remaining() != 0; j++, k++) {
                    inputBuffer.put(j, (byte) (inputBuffer.get() ^ cipherText[k]));
                }

                System.out.println("Position : " + inputBuffer.position());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputBuffer;
    }

    private void encrypt() {

        ByteBuffer inputBuffer = ByteBuffer.allocate(plainText.getBytes().length);
        ByteBuffer counterInfo = ByteBuffer.allocate(Config.BLOCK_SIZE);
        iv = Utility.secureRandomNumber(1023);
        inputBuffer.put(plainText.getBytes());
        counterInfo.put(iv);

        try {
            inputBuffer.position(0);
            int fileLength = inputBuffer.remaining() / Config.BLOCK_SIZE;
            System.out.println("File length : " + fileLength);

            for (int i = 0; i <= fileLength; i++) {
                counterInfo.put(1023, Byte.valueOf(Integer.toString(i)));

                byte[] cipherText = Utility.encript_AES(counterInfo.array(), generateKeyFromPassword().getEncoded());

                /*for (int j = 0; j < Config.BLOCK_SIZE && inputBuffer.remaining() != 0; j++) {
                    inputBuffer.put(j, (byte) (inputBuffer.get() ^ cipherText[j]));
                }*/

                for (int j = i * Config.BLOCK_SIZE, k = 0; k < Config.BLOCK_SIZE && inputBuffer.remaining() != 0; j++, k++) {
                    inputBuffer.put(j, (byte) (inputBuffer.get() ^ cipherText[k]));
                }

                System.out.println("Position : " + inputBuffer.position());
            }

            //System.out.println(Base64.getEncoder().encodeToString(inputBuffer.array()));
            //System.out.println(new String(inputBuffer.array()));
            decrypt(inputBuffer);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void decrypt(ByteBuffer cipherText) throws Exception {
        cipherText.position(0);
        int fileLength = cipherText.remaining() / Config.BLOCK_SIZE;
        ByteBuffer counterInfo = ByteBuffer.allocate(Config.BLOCK_SIZE);
        counterInfo.put(iv);

        for (int i = 0; i <= fileLength; i++) {
            counterInfo.put(1023, Byte.valueOf(Integer.toString(i)));
            byte[] cipher = Utility.encript_AES(counterInfo.array(), generateKeyFromPassword().getEncoded());

            /*for (int j = 0; j < Config.BLOCK_SIZE && cipherText.remaining() != 0; j++) {
                cipherText.put(j, (byte) (cipherText.get() ^ cipher[j]));
            }*/

            for (int j = i * Config.BLOCK_SIZE, k = 0; k < Config.BLOCK_SIZE && cipherText.remaining() != 0; j++, k++) {
                cipherText.put(j, (byte) (cipherText.get() ^ cipher[k]));
            }
        }

        System.out.println(new String(cipherText.array()));

    }

    public static ByteBuffer decrypt(ByteBuffer cipherText, byte[] iv) {
        try {
            cipherText.position(0);
            int fileLength = cipherText.remaining() / Config.BLOCK_SIZE;
            ByteBuffer counterInfo = ByteBuffer.allocate(Config.BLOCK_SIZE);
            counterInfo.put(iv);

            for (int i = 0; i <= fileLength; i++) {
                counterInfo.put(1023, Byte.valueOf(Integer.toString(i)));
                byte[] cipher = Utility.encript_AES(counterInfo.array(), generateKeyFromPassword().getEncoded());

                for (int j = i * Config.BLOCK_SIZE, k = 0; k < Config.BLOCK_SIZE && cipherText.remaining() != 0; j++, k++) {
                    cipherText.put(j, (byte) (cipherText.get() ^ cipher[k]));
                }
            }

           // System.out.println(new String(cipherText.array()));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cipherText;
    }


    public static void main(String[] args) throws Exception {
        /*Testing testing = new Testing();
        testing.encrypt();*/

        String fileName = "C:\\Users\\garik\\Documents\\test_file";
        EFS efs = new EFS(null);
        efs.username = "chetan";
        efs.password = "password";
        efs.write(fileName,0,plainText.getBytes(),"password");
        byte[] read = efs.read(fileName, 0, efs.length(fileName,efs.password), efs.password);

        System.out.println(new String(read));
    }
}
